#!/bin/bash

VERSION=$1
MAKE=$2

if [ "$VERSION" == "next" ]; then
  TXT="nightly.txt"
  N_VERSION=$( < $TXT )
  R_VERSION=$N_VERSION
  N_VERSION="${N_VERSION//./}"
  N_VERSION=$(expr $N_VERSION + 0)
  ((N_VERSION++))
  if [ $N_VERSION -lt 99 ]; then
    NN="0."
    NN=$(echo "${NN}${N_VERSION:0:1}.")
    NN=$(echo "${NN}${N_VERSION:1}")
  else
    NN=$(echo "${NN}${N_VERSION:0:1}.")
    NN=$(echo "${NN}${N_VERSION:1:1}.")
    NN=$(echo "${NN}${N_VERSION:2}")
  fi
  
  if [ "$R_VERSION" != "$NN" ]; then
    VERSION=$R_VERSION
    TXT="version.txt"
    INT=".git-fl/version"
    SHIELD="badges/version.svg"
    COLOR="#97ca00"
    LABEL="release"
    echo "${VERSION}" > $TXT
    echo "VERSION=${VERSION}" > $INT
    echo "<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" width=\"78\" height=\"20\" role=\"img\" aria-label=\"${LABEL}: ${VERSION}\"><title>${LABEL}: ${VERSION}</title><linearGradient id=\"s\" x2=\"0\" y2=\"100%\"><stop offset=\"0\" stop-color=\"#bbb\" stop-opacity=\".1\"/><stop offset=\"1\" stop-opacity=\".1\"/></linearGradient><clipPath id=\"r\"><rect width=\"78\" height=\"20\" rx=\"3\" fill=\"#fff\"/></clipPath><g clip-path=\"url(#r)\"><rect width=\"47\" height=\"20\" fill=\"#555\"/><rect x=\"47\" width=\"31\" height=\"20\" fill=\"${COLOR}\"/><rect width=\"78\" height=\"20\" fill=\"url(#s)\"/></g><g fill=\"#fff\" text-anchor=\"middle\" font-family=\"Verdana,Geneva,DejaVu Sans,sans-serif\" text-rendering=\"geometricPrecision\" font-size=\"110\"><text aria-hidden=\"true\" x=\"245\" y=\"150\" fill=\"#010101\" fill-opacity=\".3\" transform=\"scale(.1)\" textLength=\"370\">${LABEL}</text><text x=\"245\" y=\"140\" transform=\"scale(.1)\" fill=\"#fff\" textLength=\"370\">${LABEL}</text><text aria-hidden=\"true\" x=\"615\" y=\"150\" fill=\"#010101\" fill-opacity=\".3\" transform=\"scale(.1)\" textLength=\"210\">${VERSION}</text><text x=\"615\" y=\"140\" transform=\"scale(.1)\" fill=\"#fff\" textLength=\"210\">${VERSION}</text></g></svg>" > $SHIELD
  fi

  if [ "$N_VERSION" != "$NN" ]; then
    VERSION=$NN
    TXT="nightly.txt"
    SHIELD="badges/nightly.svg"
    COLOR="#007BFF"
    LABEL="nightly"
    echo "${VERSION}" > $TXT
    echo "<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" width=\"78\" height=\"20\" role=\"img\" aria-label=\"${LABEL}: ${VERSION}\"><title>${LABEL}: ${VERSION}</title><linearGradient id=\"s\" x2=\"0\" y2=\"100%\"><stop offset=\"0\" stop-color=\"#bbb\" stop-opacity=\".1\"/><stop offset=\"1\" stop-opacity=\".1\"/></linearGradient><clipPath id=\"r\"><rect width=\"78\" height=\"20\" rx=\"3\" fill=\"#fff\"/></clipPath><g clip-path=\"url(#r)\"><rect width=\"47\" height=\"20\" fill=\"#555\"/><rect x=\"47\" width=\"31\" height=\"20\" fill=\"${COLOR}\"/><rect width=\"78\" height=\"20\" fill=\"url(#s)\"/></g><g fill=\"#fff\" text-anchor=\"middle\" font-family=\"Verdana,Geneva,DejaVu Sans,sans-serif\" text-rendering=\"geometricPrecision\" font-size=\"110\"><text aria-hidden=\"true\" x=\"245\" y=\"150\" fill=\"#010101\" fill-opacity=\".3\" transform=\"scale(.1)\" textLength=\"370\">${LABEL}</text><text x=\"245\" y=\"140\" transform=\"scale(.1)\" fill=\"#fff\" textLength=\"370\">${LABEL}</text><text aria-hidden=\"true\" x=\"615\" y=\"150\" fill=\"#010101\" fill-opacity=\".3\" transform=\"scale(.1)\" textLength=\"210\">${VERSION}</text><text x=\"615\" y=\"140\" transform=\"scale(.1)\" fill=\"#fff\" textLength=\"210\">${VERSION}</text></g></svg>" > $SHIELD
  fi
else
  if [ "$MAKE" == "n" ]; then
    TXT="nightly.txt"
    SHIELD="badges/nightly.svg"
    COLOR="#007BFF"
    LABEL="nightly"
    echo "${VERSION}" > $TXT
  else
    TXT="version.txt"
    INT=".git-fl/version"
    SHIELD="badges/version.svg"
    COLOR="#97ca00"
    LABEL="release"
    echo "${VERSION}" > $TXT
    echo "VERSION=${VERSION}" > $INT
  fi
  echo "<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" width=\"78\" height=\"20\" role=\"img\" aria-label=\"${LABEL}: ${VERSION}\"><title>${LABEL}: ${VERSION}</title><linearGradient id=\"s\" x2=\"0\" y2=\"100%\"><stop offset=\"0\" stop-color=\"#bbb\" stop-opacity=\".1\"/><stop offset=\"1\" stop-opacity=\".1\"/></linearGradient><clipPath id=\"r\"><rect width=\"78\" height=\"20\" rx=\"3\" fill=\"#fff\"/></clipPath><g clip-path=\"url(#r)\"><rect width=\"47\" height=\"20\" fill=\"#555\"/><rect x=\"47\" width=\"31\" height=\"20\" fill=\"${COLOR}\"/><rect width=\"78\" height=\"20\" fill=\"url(#s)\"/></g><g fill=\"#fff\" text-anchor=\"middle\" font-family=\"Verdana,Geneva,DejaVu Sans,sans-serif\" text-rendering=\"geometricPrecision\" font-size=\"110\"><text aria-hidden=\"true\" x=\"245\" y=\"150\" fill=\"#010101\" fill-opacity=\".3\" transform=\"scale(.1)\" textLength=\"370\">${LABEL}</text><text x=\"245\" y=\"140\" transform=\"scale(.1)\" fill=\"#fff\" textLength=\"370\">${LABEL}</text><text aria-hidden=\"true\" x=\"615\" y=\"150\" fill=\"#010101\" fill-opacity=\".3\" transform=\"scale(.1)\" textLength=\"210\">${VERSION}</text><text x=\"615\" y=\"140\" transform=\"scale(.1)\" fill=\"#fff\" textLength=\"210\">${VERSION}</text></g></svg>" > $SHIELD
fi
