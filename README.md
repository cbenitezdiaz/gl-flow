# GL Flow

GL Flow is a set of commands that groups git functions adapted to gitlab

[![Release](badges/version.svg?)](https://gitlab.com/cbenitezdiaz/gl-fl)
[![Nightly](badges/nightly.svg?)](https://gitlab.com/cbenitezdiaz/gl-fl/-/tree/nightly)

## CONFIG

### Git

So that git does not ask for username and password, you must edit the config file found in the .git folder of the current project. This if you want to do it manually, you can use the git remote command as in the following example.

```bash
$ git remote set-url origin https://USERNAME:ACCESS_TOKEN@gitlab.domain.com/USERNAME/REPOSITORY.git
```

Or activate git with ssh

### Dependencies

- open || xdg-open
- curl
- sed, awk

Before starting you must have these environment variables configured on your system

On GNU/Linux|Mac:

```bash
$ vim .profile|.bash_profile|.bashrc|.zshrc
```
- `export PATH="$HOME/.git-fl:$PATH"`

Run the following commands:

```bash
$ git clone http://gitlab.com/cbenitezdiaz/gl-fl.git && cd gl-fl && ./install.sh
```

## Use

### Help

To see the help run the following command:

```bash
$ git fl -h
```

**Output:**

<br>

```bash
Version: x.x.x

usage: git fl <option>

Available options are:
   help      -h     Shows help and use of subcommands.
   init      -i     Initialize a new git repo with support for the branching model.
   config    -c     Show git fl configuration.
   merge     -m     Merge incoming changes from main branch to current branch.
   start     -s     Initialize a new branch from the main branch.
   push      -p     Submit the branch changes to the repository.
   finish    -f     Delete the branch created locally and the remote one.
   update    -u     Update if a new version is available.
   version   -v     Show installed version.
   vc tasks  -k     Install vs-code tasks.json.

Try 'git fl <option> help' for details.
```

### Init

Start setup:

```bash
$ git fl -i
```

Show settings:

```bash
$ git fl -c
```

Output:

```bash
========================================
           CONFIGURATION

========================================
[*] GIT: https://gitlab.com/gitlab.username/gl-fl
[*] Main branch: dev
[*] Branch: feature/gitlab.username/20210914/T-1000

========================================
```

## Feature|Bug|Hotfix

### Start feature|bugfix|hotfix

To start a new feature|bugfix|hotfix you just have to run the following command and add a name at the end.

```bash
$ git fl -s -f [name]
```

### Publish feature|bugfix|hotfix with changes

To send your changes to the repository execute the following changes.

```bash
$ git add [file|.]

$ git fl -p [message] [assign]
```

Attention: check for new changes in the branch before submitting your changes

### Finish feature|bugfix|hotfix branch

At the end of a feature|bugfix|hotfix this branch is removed and it becomes master again. Remember to re-update master branch to get the latest changes.

```bash
$ git fl -f
```

<br>

# Check incoming changes

Using git fl

```bash
$ git fl -m
```

Or just git

```bash
$ git switch test

$ git pull

$ git switch created_branch

$ git reset --hard

$ git clean -df

$ git merge dev --no-commit --no-ff

$ git status

$ git commit
```

<br>

# TO DO 2023

* [ ] MR Comment Template
* [ ] MR fl to other branch (main -> dev -> stg)
* [ ] Prettier
