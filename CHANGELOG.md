# **Change Log**
## [2022-06-25]

- [342775d](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/342775d33b18cc8e132841447c8f36fa13b9a7c9) update, tasks and new version by @cbenitezdiaz [](/)
### Added
- [161847b](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/161847bdc66ae313d2b4beb87e60e842e21f649e) change folder name by @cbenitezdiaz [update](/update)
- [c609d68](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/c609d68148e555f2eb8f6b294f0e90e641c85d87) add u arg by @cbenitezdiaz [update](/update)
- [1617a42](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/1617a42d7575e61c848af9e147c8491c6f8092e9) update, tasks and new version by @cbenitezdiaz [update](/update)

### Fixed

---
## [2022-06-24]

- [5edd314](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/5edd314eec0d71c5717950f64594141c141e6985) add slack notification by @cbenitezdiaz [](/)
### Added
- [acfbc88](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/acfbc88179027ffc42f5648a7833d6e259df7278) new badges by @cbenitezdiaz [new-version](/new-version)
- [23676f4](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/23676f40ac9665bb65ddee80384136872e3a5cbf) update file version by @cbenitezdiaz [new-version](/new-version)
- [a162471](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/a1624712e582900a56c86dca5a7ed3d2eab3070b) add merge tasks.json by @cbenitezdiaz [fl-wip](/fl-wip)
- [94303a3](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/94303a37e9e6ee8edfe984ac9e2816ccb562fe25) output to dev null by @cbenitezdiaz [fl-wip](/fl-wip)
- [e813b29](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/e813b297ed6937937a6ec9475fa2f9296f184b72) update vscode-tasks.json by @cbenitezdiaz [fl-wip](/fl-wip)
- [9de52eb](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/9de52eb46dfb24097d3e4bac0e57923b82bc2aef) add notify silent mode by @cbenitezdiaz [fl-wip](/fl-wip)
- [d1b21f9](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/d1b21f9c4087121a26734757edf82cabebd3ce60) fix notify wip by @cbenitezdiaz [fl-wip](/fl-wip)
- [70f65f0](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/70f65f045e1877fe598b195aa7475b97e2b4cd5b) fix notify wip by @cbenitezdiaz [fl-wip](/fl-wip)
- [765ca23](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/765ca2340dcda5ad1ea5d4dcf0edc78019bc5bcc) fix notify wip by @cbenitezdiaz [fl-wip](/fl-wip)
- [17fa766](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/17fa766b2e92e32a0d6a042f6deee982d6fb0ed9) fix notify wip by @cbenitezdiaz [fl-wip](/fl-wip)
- [fa4fed3](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/fa4fed39653ad0dfb5b8e13e84fe636cd27e2f3b) fix notify wip by @cbenitezdiaz [fl-wip](/fl-wip)
- [875b0c9](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/875b0c939b764bf9691c64f639d60a2333aa7d1e) fix notify wip by @cbenitezdiaz [fl-wip](/fl-wip)
- [ab4a285](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/ab4a28518c1bd72cf70a6530b18eab2ce7838413) fix notify wip by @cbenitezdiaz [fl-wip](/fl-wip)
- [647ed80](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/647ed80a0ce0107ad9b43db32fd62836fd672965) fix notify wip by @cbenitezdiaz [fl-wip](/fl-wip)
- [4f4adbd](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/4f4adbd5e167f8864e2143510f55c28f74513eb9) fix notify wip by @cbenitezdiaz [fl-wip](/fl-wip)
- [628814f](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/628814f188001139b705dcaf843f2a4cf86b7940) fix notify wip by @cbenitezdiaz [fl-wip](/fl-wip)
- [a6ca174](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/a6ca174bf574b0fca83019f8d78db759e6801334) fix notify wip by @cbenitezdiaz [fl-wip](/fl-wip)
- [a8a63ae](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/a8a63ae77f7a4692603e1afc73f4ca5b776d4fe9) fix notify by @cbenitezdiaz [fl-wip](/fl-wip)
- [f712ca5](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/f712ca5ea3a17644b3bf454c327afddfed6fa326) test notify by @cbenitezdiaz [fl-wip](/fl-wip)
- [7655d50](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/7655d506eeaff22d3028453b7bf7adb270c1642a) new branch by @cbenitezdiaz [fl-wip](/fl-wip)

### Fixed

---

## [2022-06-20]

### Added
- [02521c0](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/02521c0c7e0d57bca5113ddbc507a87c40eed1ae) fix finish branch by @cbenitezdiaz [T-1000](/T-1000)
- [f052db2](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/f052db20e8cabcab6372b4c6f47d040856fdc491) update installer by @cbenitezdiaz [T-1000](/T-1000)
- [b9a36b4](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/b9a36b439eb26a9a964ea5de7fa3b8c1f83fed34) minor changes by @cbenitezdiaz [T-1000](/T-1000)
- [584ce82](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/584ce828be2fd703e8615216ac2921a849ce3fba) new config and other stuff by @cbenitezdiaz [T-1000](/T-1000)
- [6b9b7e3](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/6b9b7e3c4aeb2552e3d38ba9c540f34b8ef34ffb) version.txt add and other fix by @cbenitezdiaz [T-1000](/T-1000)
- [560ad55](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/560ad558bd97dbbe92fc51069eda737f3663752b) version fix by @cbenitezdiaz [T-1000](/T-1000)
- [296ada4](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/296ada408ebb06aec66cbba9542955e3f38de73e) minor changes by @cbenitezdiaz [T-1000](/T-1000)
- [6f080ed](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/6f080ed35c916c9bb972d64acf5a1dc48d9f1e9a) update readme by @cbenitezdiaz [T-1000](/T-1000)
- [3db30f3](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/3db30f3134e7960b3a462fcac9a3228a550acd64) badget nigthtly by @cbenitezdiaz [T-1000](/T-1000)
- [544007a](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/544007a16630ea0a390acd8e719aa21c73d6b5a0) badget version created by @cbenitezdiaz [T-1000](/T-1000)
- [d11ddf2](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/d11ddf23b9583bf92f03e18708305bd0fd8a6a12) update readme and other stuff by @cbenitezdiaz [T-1000](/T-1000)
- [92c6d39](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/92c6d3931c0b3715faff2716340f0b76ac745992) new usage and other minor changes by @cbenitezdiaz [T-1000](/T-1000)
- [01d9b2e](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/01d9b2e0b583d2680a4a70bc2137b041484ddcd1) multiple changes by @cbenitezdiaz [T-1000](/T-1000)
- [a1847fc](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/a1847fcf94d1eaf4073553cc4cdadcc5904d3359) test summary 5 by @cbenitezdiaz [T-1000](/T-1000)
- [5608750](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/5608750758707887da315b04487e39c57f74d173) test summary 4 by @cbenitezdiaz [T-1000](/T-1000)
- [f8f25ba](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/f8f25ba6333e1483c53755e1b98da6956c002e2b) test summary 2 by @cbenitezdiaz [T-1000](/T-1000)
- [767f024](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/767f0243985f6bff160503b9a64ce4e2814114a2) test summary by @cbenitezdiaz [T-1000](/T-1000)
- [fc52ba9](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/fc52ba937f0c51185910bb68be21d6c1d9e1bc88) minor changes 4 by @cbenitezdiaz [T-1000](/T-1000)
- [d7f73d4](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/d7f73d45624f726e5b81d9d0550f3a65ca724fef) minor changes 3 by @cbenitezdiaz [T-1000](/T-1000)
- [e31416a](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/e31416a7087636c0d9fe10c64694d48b410104a5) minor changes by @cbenitezdiaz [T-1000](https://jira.tigo.com.hn/browse/T-1000T-1000)
- [aaafe4a](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/aaafe4ac782024f1fb445e45fbc55e7c7b1790f7) minor changes by @cbenitezdiaz [T-1000](https://jira.tigo.com.hn/browse/T-1000T-1000)
- [205028c](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/205028c3d972c78662703f00b0c4afb2fe3ff901) change ticket url 1 by @cbenitezdiaz [T-1000](https://jira.tigo.com.hn/browse/T-1000T-1000)
- [8cfd634](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/8cfd634234898ea115e889a4709d247ad07b437b) change ticket url by @cbenitezdiaz [T-1000](https://jira.tigo.com.hn/browse/T-1000T-1000)
- [7a2f52d](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/7a2f52d43f0a5e0771f161f84cf98e39a32f02f7) minor changes by @cbenitezdiaz [T-1000](https://jira.tigo.com.hn/browse/T-1000T-1000)
- [276ab55](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/276ab55bef48b6c2e1f09088b6f14ecdadc833d0) minor changes by @christian.benitez [T-1000](T-1000)
- [89605f3](https://gitlab.com/cbenitezdiaz/gl-flow/-/commit/89605f3d77195653324b7284c27b8b0e44866ca3) minor changes by @ [T-1000](https://jira.tigo.com.hn/browse/T-1000)

### Fixed

---



















































