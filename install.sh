#!/bin/bash

# Setup the GITFLOW_DIR for different operating systems.
# This is mostly to make sure that we get the correct directory when the
# git-fl file is a symbolic link

# Colors
greenColor="\033[0;32m\033[1m"
redColor="\033[0;31m\033[1m"
blueColor="\033[0;34m\033[1m"
yellowColor="\033[0;33m\033[1m"
purpleColor="\033[0;35m\033[1m"
turquoiseColor="\033[0;36m\033[1m"
grayColor="\033[0;37m\033[1m"
endColor="\033[0m\033[0m"

# Paths & Files
GITFLOW_DIR="$HOME/.git-fl"
GITFLOW_FILE="$GITFLOW_DIR/git-fl"

# Check if file exists and remove previous installation
if [ -f "$GITFLOW_FILE" ]; then
  rm -rf $GITFLOW_DIR 2>/dev/null
  echo -e "\n${yellowColor}[*]${endColor} ${grayColor}Removing previous installation...${endColor}"
  sleep 1
fi

# Copy the new installation
echo -e "\n${yellowColor}[*]${endColor} ${grayColor}Installing GL Flow${endColor}"
cp -r .git-fl $GITFLOW_DIR 2>/dev/null
sleep 2

# Check if file exists for new or previous installation
if [ -f "$GITFLOW_FILE" ]; then
  echo -e "\n${greenColor}[*] Installation complete!${endColor}\n"
  sleep 1
else
  echo -e "\n${redColor}[!] Error: installation failed${endColor}\n"
  echo -e "\t${redColor}- Verify that you have execute permissions${endColor}\n"
  echo -e "\t${redColor}- Verify that the HOME path exists${endColor}\n\n"
  sleep 1
  exit 1
fi

# Check all dependencies
echo -e "${yellowColor}[!] Searching dependencies ${endColor}"

if command -v xdg-open > /dev/null 2>&1; then
  echo -e "    ${greenColor}[Ok]${endColor} xdg-open"
elif command -v open > /dev/null 2>&1; then
  echo -e "    ${greenColor}[Ok]${endColor} open"
else
  echo -e "    ${redColor}[X]${endColor} xdg-open or open are not installed"
fi

if command -v curl > /dev/null 2>&1; then
  echo -e "    ${greenColor}[Ok]${endColor} curl"
else
  echo -e "    ${redColor}[X]${endColor} curl is not installed"
fi

if command -v xmlstarlet > /dev/null 2>&1; then
  echo -e "    ${greenColor}[Ok]${endColor} xmlstarlet"
else
  echo -e "    ${redColor}[X]${endColor} xmlstartlet is not installed"
fi

if command -v sed > /dev/null 2>&1; then
  echo -e "    ${greenColor}[Ok]${endColor} sed"
else
  echo -e "    ${redColor}[X]${endColor} sed is not installed"
fi

if command -v awk > /dev/null 2>&1; then
  echo -e "    ${greenColor}[Ok]${endColor} awk"
else
  echo -e "    ${redColor}[X]${endColor} awk is not installed"
fi
